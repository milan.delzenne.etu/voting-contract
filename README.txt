Introduction

Ce projet est un contrat intelligent Ethereum qui permet aux utilisateurs de voter sur des propositions. Le contrat comprend les fonctionnalités suivantes :

Inscription des électeurs
Inscription des propositions
Vote sur les propositions
Installation

Pour installer le contrat, vous pouvez suivre les instructions suivantes :

Clonez le dépôt Git :
git clone https://github.com/bard/voting-contract

Accédez au répertoire du projet :
cd voting-contract

Installez les dépendances :
npm install

Compilation

Pour compiler le contrat, vous pouvez exécuter la commande suivante :
npx truffle compile

Déploiement

Pour déployer le contrat, vous pouvez exécuter la commande suivante :
npx truffle deploy

Utilisation

Pour utiliser le contrat, vous pouvez suivre les instructions suivantes :

Inscription d'un électeur

Pour vous inscrire en tant qu'électeur, vous devez fournir votre adresse Ethereum. Vous pouvez utiliser la commande suivante pour obtenir votre adresse Ethereum :
npx truffle wallet getAddress

Une fois que vous avez votre adresse Ethereum, vous pouvez vous inscrire en tant qu'électeur en exécutant la commande suivante :
npx truffle run --network rinkeby registerVoter --address <votant>

Inscription d'une proposition

Pour inscrire une proposition, vous devez fournir une description de la proposition. Vous pouvez utiliser la commande suivante pour inscrire une proposition :
npx truffle run --network rinkeby registerProposal --description <description>

Vote sur une proposition

Pour voter sur une proposition, vous devez fournir les identifiants des propositions sur lesquelles vous souhaitez voter. Vous pouvez utiliser la commande suivante pour voter sur une proposition :
npx truffle run --network rinkeby vote --address <votant> --proposalIds <id1> <id2> ...